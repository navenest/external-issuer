package v1alpha1

type CertificateSource struct {
	URL string `json:"url"`

	AuthSecretName  string `json:"authSecretName"`
	SecretNamespace string `json:"secretNamespace,omitempty"`
}
