package controller

import (
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"go.uber.org/zap"
	"k8s.io/apimachinery/pkg/types"
	"math/big"
	"net/http"
	"os"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"time"

	corev1 "k8s.io/api/core/v1"
	ctrl "sigs.k8s.io/controller-runtime"

	cmapi "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	api "gitlab.com/navenest/external-issuer/api/v1alpha1"

	"github.com/cert-manager/issuer-lib/api/v1alpha1"
	"github.com/cert-manager/issuer-lib/controllers"
	"github.com/cert-manager/issuer-lib/controllers/signer"
)

// +kubebuilder:rbac:groups=cert-manager.io,resources=certificaterequests,verbs=get;list;watch
// +kubebuilder:rbac:groups=cert-manager.io,resources=certificaterequests/status,verbs=patch

// +kubebuilder:rbac:groups=certificates.k8s.io,resources=certificatesigningrequests,verbs=get;list;watch
// +kubebuilder:rbac:groups=certificates.k8s.io,resources=certificatesigningrequests/status,verbs=patch
// +kubebuilder:rbac:groups=certificates.k8s.io,resources=signers,verbs=sign,resourceNames=issuers.http-external-issuer.navenest.com/*;clusterissuers.http-external-issuer.navenest.com/*

// +kubebuilder:rbac:groups=http-external-issuer.navenest.com,resources=issuers;clusterissuers,verbs=get;list;watch
// +kubebuilder:rbac:groups=http-external-issuer.navenest.com,resources=issuers/status;clusterissuers/status,verbs=patch

// +kubebuilder:rbac:groups=core,resources=events,verbs=create;patch
// +kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch

type Signer struct {
	MaxRetryDuration time.Duration

	client client.Client
}

type httpClientConfig struct {
	Credentials string
}

const inClusterNamespacePath = "/var/run/secrets/kubernetes.io/serviceaccount/namespace"

func (s Signer) SetupWithManager(ctx context.Context, mgr ctrl.Manager) error {
	if err := cmapi.AddToScheme(mgr.GetScheme()); err != nil {
		return err
	}

	if err := api.AddToScheme(mgr.GetScheme()); err != nil {
		return err
	}

	s.client = mgr.GetClient()

	return (&controllers.CombinedController{
		IssuerTypes:        []v1alpha1.Issuer{&api.Issuer{}},
		ClusterIssuerTypes: []v1alpha1.Issuer{&api.ClusterIssuer{}},

		FieldOwner:       "http-external-issuer.navenest.com",
		MaxRetryDuration: 1 * time.Minute,

		Sign:          s.Sign,
		Check:         s.Check,
		EventRecorder: mgr.GetEventRecorderFor("http-external-issuer.navenest.com"),
	}).SetupWithManager(ctx, mgr)
}

func (s *Signer) extractCertificateSourceSpec(obj client.Object) (certificateSourceSpec *api.CertificateSource, namespace string) {
	switch t := obj.(type) {
	case *api.Issuer:
		ctrl.Log.Info("returning Issuer")
		return &t.Spec, t.Namespace
	case *api.ClusterIssuer:
		ctrl.Log.Info("returning cluster issuer")
		clusterNamespace, err := os.ReadFile(inClusterNamespacePath)
		if err != nil {
			ctrl.Log.Error(err, "Unable to read namespace")
		}

		return &t.Spec, string(clusterNamespace)
	}

	panic("Program Error: Unhandled issuer type")
}

func (s *Signer) Check(ctx context.Context, issuerObject v1alpha1.Issuer) error {
	return nil
}

func (s *Signer) Sign(ctx context.Context, cr signer.CertificateRequestObject, issuerObject v1alpha1.Issuer) (signer.PEMBundle, error) {
	certificateSourceSpec, resourceNamespace := s.extractCertificateSourceSpec(issuerObject)
	ctrl.Log.Info(resourceNamespace)
	// generate random ca private key
	caPrivateKey, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	credentials, err := s.getCredentials(ctx, resourceNamespace, certificateSourceSpec)

	req, err := http.NewRequest(http.MethodGet, certificateSourceSpec.URL, nil)
	if err != nil {
		panic(err)
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}
	ctrl.Log.Info("response", zap.Any("status", res.Status))
	ctrl.Log.Info("credentials",
		zap.Any("creds", credentials))

	if err != nil {
		return signer.PEMBundle{}, err
	}

	caCRT := &x509.Certificate{
		SerialNumber: big.NewInt(1),
		Subject: pkix.Name{
			Organization: []string{"Acme Co"},
		},
		NotBefore: time.Now(),
		NotAfter:  time.Now().Add(time.Hour * 24 * 180),

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	// load client certificate request
	clientCRTTemplate, _, _, err := cr.GetRequest()
	if err != nil {
		return signer.PEMBundle{}, err
	}

	// create client certificate from template and CA public key
	clientCRTRaw, err := x509.CreateCertificate(rand.Reader, clientCRTTemplate, caCRT, clientCRTTemplate.PublicKey, caPrivateKey)
	if err != nil {
		panic(err)
	}

	clientCrt := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: clientCRTRaw})
	return signer.PEMBundle{
		ChainPEM: clientCrt,
	}, nil
}

func (s *Signer) getCredentials(ctx context.Context, resourceNamespace string, httpCertificateSource *api.CertificateSource) (httpClientConfig, error) {
	var clientConfig httpClientConfig
	ctrl.Log.Info("secretNamespace Info" + httpCertificateSource.AuthSecretName + " " + resourceNamespace)
	secretNamespaceName := types.NamespacedName{
		Name:      httpCertificateSource.AuthSecretName,
		Namespace: resourceNamespace,
	}
	var secret corev1.Secret
	ctrl.Log.Info("pulling creds")
	if err := s.client.Get(ctx, secretNamespaceName, &secret); err != nil {
		ctrl.Log.Error(err, "Unable to read message")
		return clientConfig, err
	}
	ctrl.Log.Info("creds pulled")
	for i, v := range secret.Data {
		ctrl.Log.Info(i)
		ctrl.Log.Info(string(v))
	}
	credentials, exists := secret.Data["token"]
	if !exists {
		ctrl.Log.Info("No credentials found")
		return clientConfig, fmt.Errorf("no credentials found in secret %s under %s", secretNamespaceName, "token")
	}
	ctrl.Log.Info("got credentials")
	clientConfig.Credentials = string(credentials)

	return clientConfig, nil
}
